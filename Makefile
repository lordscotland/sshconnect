ARCHS = armv6 armv7 arm64

include theos/makefiles/common.mk

TWEAK_NAME = SSHConnect
SSHConnect_FILES = Tweak.x
SSHConnect_FRAMEWORKS = UIKit
SSHConnect_PRIVATE_FRAMEWORKS = Preferences

include $(THEOS_MAKE_PATH)/tweak.mk

TOOL_NAME = enableSSH
enableSSH_FILES = helper.c

include $(THEOS_MAKE_PATH)/tool.mk

after-enableSSH-stage::
	@chmod -v 4755 $(THEOS_STAGING_DIR)$(LOCAL_INSTALL_PATH)/$(THEOS_CURRENT_INSTANCE)$(TARGET_EXE_EXT)
