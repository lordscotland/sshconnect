#import <Preferences/Preferences.h>
#import <objc/runtime.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#include <launch.h>

#define HELPER_EXE "/usr/bin/enableSSH"
#define INSERT_POS 3

static BOOL $_isSSHEnabled() {
  BOOL enabled=NO;
  launch_data_t msg=launch_data_new_string(LAUNCH_KEY_GETJOBS),data=launch_msg(msg);
  launch_data_free(msg);
  if(data!=NULL){
    if(launch_data_get_type(data)==LAUNCH_DATA_DICTIONARY
     && launch_data_dict_lookup(data,"com.openssh.sshd")!=NULL){enabled=YES;}
    launch_data_free(data);
  }
  return enabled;
}
static int $_compare(PSSpecifier** spec1,PSSpecifier** spec2) {
  return [(*spec1).name localizedCompare:(*spec2).name]?:spec1-spec2;
}

@interface SSHListController : PSListController @end
@implementation SSHListController
-(NSArray*)specifiers {
  if(!_specifiers){
    NSMutableArray* specs=[NSMutableArray array];
    NSMutableArray* xspecs=[NSMutableArray array];
    struct ifaddrs* addrs;
    if(getifaddrs(&addrs)==0){
      struct ifaddrs* addr=addrs;
      for (;addr!=NULL;addr=addr->ifa_next){
        int af=addr->ifa_addr->sa_family;
        char addrstr[INET6_ADDRSTRLEN];
        if((af==AF_INET || af==AF_INET6) && inet_ntop(af,
         &((struct sockaddr_in*)addr->ifa_addr)->sin_addr,addrstr,INET6_ADDRSTRLEN)!=NULL){
          PSSpecifier* spec=[PSSpecifier
           preferenceSpecifierNamed:[NSString stringWithUTF8String:addr->ifa_name]
           target:self set:nil get:@selector(valueForSpecifier:)
           detail:nil cell:PSTitleValueCell edit:nil];
          [spec setProperty:[NSString stringWithUTF8String:addrstr] forKey:@"value"];
          [spec setProperty:(NSNumber*)kCFBooleanTrue forKey:@"isCopyable"];
          [(addr->ifa_flags&3)==3?specs:xspecs addObject:spec];
        }
      }
      freeifaddrs(addrs);
    }
    NSUInteger count=specs.count,xcount=xspecs.count,i=0;
    id* list=malloc(sizeof(PSSpecifier*)*(2+(count?count+1:0)+(xcount?xcount+1:0)));
    list[i++]=[PSSpecifier emptyGroupSpecifier];
    list[i++]=[PSSpecifier preferenceSpecifierNamed:@"SSH"
     target:self set:@selector(setEnabled:forSpecifier:)
     get:@selector(isEnabled) detail:nil cell:PSSwitchCell edit:nil];
    NSBundle* bundle=[NSBundle bundleWithPath:
     @"/System/Library/AccessibilityBundles/MobileTimer.axbundle"];
    if(count){
      list[i++]=[PSSpecifier groupSpecifierWithName:[bundle
       localizedStringForKey:@"alarm.active" value:nil table:@"Accessibility"]];
      [specs getObjects:list+i range:NSMakeRange(0,count)];
      qsort(list+i,count,sizeof(PSSpecifier*),(const void*)$_compare);
      i+=count;
    }
    if(xcount){
      list[i++]=[PSSpecifier groupSpecifierWithName:[bundle
       localizedStringForKey:@"alarm.inactive" value:nil table:@"Accessibility"]];
      [xspecs getObjects:list+i range:NSMakeRange(0,xcount)];
      qsort(list+i,xcount,sizeof(PSSpecifier*),(const void*)$_compare);
      i+=xcount;
    }
    _specifiers=[[NSArray alloc] initWithObjects:list count:i];
    free(list);
  }
  return _specifiers;
}
-(NSString*)valueForSpecifier:(PSSpecifier*)spec {
  return [spec propertyForKey:@"value"];
}
-(CFBooleanRef)isEnabled {
  return $_isSSHEnabled()?kCFBooleanTrue:kCFBooleanFalse;
}
-(void)setEnabled:(NSNumber*)enabled forSpecifier:(PSSpecifier*)spec {
  FILE* ph=popen(enabled.boolValue?HELPER_EXE:HELPER_EXE" -","r");
  NSFileHandle* file=[[NSFileHandle alloc] initWithFileDescriptor:fileno(ph)];
  NSData* output=[file readDataToEndOfFile];
  [file release];
  int status=pclose(ph);
  if(status || output.length){
    NSString* message=[[NSString alloc] initWithData:output encoding:NSUTF8StringEncoding];
    UIAlertView *alert=[[UIAlertView alloc]
     initWithTitle:[NSString stringWithFormat:@"Error %d",status>>8]
     message:message delegate:nil
     cancelButtonTitle:[[NSBundle mainBundle]
     localizedStringForKey:@"Cancel" value:nil table:nil]
     otherButtonTitles:nil];
    [message release];
    [alert show];
    [alert release];
    [self reloadSpecifier:spec];
  }
  spec=self.specifier;
  [spec.target reloadSpecifier:spec];
}
@end

%hook PrefsListController
%new(@:@)
-(NSString*)valueForSSHSpecifier {
  return [[NSBundle bundleWithIdentifier:@"com.apple.UIKit"]
   localizedStringForKey:$_isSSHEnabled()?@"On":@"Off" value:nil table:nil];
}
-(NSMutableArray*)specifiers {
  NSMutableArray* specifiers=%orig;
  if(!objc_getAssociatedObject(self,$_isSSHEnabled)){
    objc_setAssociatedObject(self,$_isSSHEnabled,(void*)1,OBJC_ASSOCIATION_ASSIGN);
    PSSpecifier* spec=[PSSpecifier preferenceSpecifierNamed:@"SSH"
     target:self set:nil get:@selector(valueForSSHSpecifier)
     detail:[SSHListController class] cell:PSLinkListCell edit:nil];
    [spec setProperty:[UIImage imageNamed:
     (floor(NSFoundationVersionNumber)<=NSFoundationVersionNumber_iOS_6_1)?
     @"SSHConnect-pre7.png":@"SSHConnect.png"] forKey:@"iconImage"];
    [specifiers insertObject:spec atIndex:INSERT_POS];
  }
  return specifiers;
}
%end
